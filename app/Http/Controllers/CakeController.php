<?php

namespace App\Http\Controllers;

use App\Cake;
use Illuminate\Support\Facades\App;

class CakeController extends Controller
{
    public function index()
    {

        // $cakes = Cake::all();
        //$cakes = Cake::orderBy('name', 'desc')->get();
        // $cakes = Cake::where('type', 'American')->get();
        $cakes = Cake::latest()->get();

        return view('cakes.index', ['cakes' => $cakes]);

    }

    public function show($id)
    {

        $cakes = Cake::findOrFail($id);

        return view('cakes.show', ['cakes' => $cakes]);

    }

    public function create()
    {

        return view('cakes.create');

    }
    public function store()
    {

        $cakes = new Cake();
        $cakes->name = request('name');
        $cakes->base = request('base');
        $cakes->type = request('type');
        $cakes->toppings = request('toppings');
        $cakes->save();

        return redirect('/')->with('mssg', 'Thanks for your order');

    }

    public function destroy($id)
    {
        $cakes = Cake::findOrFail($id);
        $cakes->delete();
        return redirect('/');

    }
}
