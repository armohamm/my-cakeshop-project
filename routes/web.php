<?php

use App\Http\Controllers\CakeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () { //*   /:the router that we expect like www.com/   and welcome is in resources->views  */

    return view('welcome');
});

Route::get('/cakes','CakeController@index');

Route::get('/cakes/create','CakeController@create');

Route::post('/cakes','CakeController@store');

Route::get('/cakes/{id}','CakeController@show');

Route::delete('/cakes/{id}','CakeController@destroy');





