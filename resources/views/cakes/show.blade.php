@extends('layouts.layout')

@section('content')
<div class="wrapper cakes_details" >

    <h1>order for</h1>{{ $cakes->name }}
    <h1>type</h1>{{ $cakes->type}}
    <h1> base</h1>{{ $cakes->base }}
    <p class="toppings">
        <ul>
            @foreach ($cakes->toppings as $topping)
            <li>{{ $topping }}</li>

            @endforeach

        </ul>
        <form action="/cakes/{{ $cakes->id }}"  method="POST">
        @csrf
        @method('DELETE')
        <button>Delete order</button>



        </form>
    </p>
    <a href="/cakes" class="back">BACK TO you main page</a>

</div>
@endsection
