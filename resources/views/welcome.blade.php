@extends('layouts.layout')

@section('content')

        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">

                <img src="\img\images.jpg" alt="cakes house logo">
                <div class="title m-b-md">
                    Cake House </br>
                    The world's most delicious cake
                </div>
                <a href="cakes/create">PLcae your order</a>
                <p class="mssg">{{ session('mssg') }}</p>
            </ul>



            </form>

            </div>
        </div>
@endsection
